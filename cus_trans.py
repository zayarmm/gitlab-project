from datetime import datetime, timedelta
import csv

from pymongo import MongoClient

client = MongoClient("mongodb+srv://reportuser:dOFX68CX2wx1D9UP@ayapayreportingnew.iigql.mongodb.net/?retryWrites=true&w=majority")

customers = client\
.get_database('ayapay')\
.get_collection('customer')\
.find({
    "offer": "5d0c93408fe40f29d82e4053",
    "status": "active",
    "createdAt": {"$gte": datetime(2019, 1, 1)}
}, {
    "phone": 1,
    "name": 1,
    "status": 1,
    "offer": 1,
    "offer_name": "Subscriber Level 2",
    "nrc": {"$concat": [{"$toString": "$nric.code" }, "/", "$nric.name", "(", "$nric.type", ")", {"$toString": "$nric.number"}]},
    "createdAt": 1
}).sort({
    "createdAt": 1
})

f = open("InactiveCustomersPart1.csv", "w", newline="")

field_names = [
    '_id',
    'name',
    'phone',
    'status',
    'offer',
    'createdAt',
    'offer_name',
    'nrc',
    'origAmount',
    'balance',
    'accountName',
    'accountNo',
    'accountNoShowed'
]

writer = csv.DictWriter(f, field_names)
writer.writeheader()

counter = 0

for cust in customers:
    cust_id = cust['_id']

    txns = client\
    .get_database("ayapay")\
    .get_collection("transaction")\
    .find({
        "$or": [{"sender.id": str(cust_id)}, {"receiver.id": str(cust_id)}]
    }, {
        "_id": 0,
        "createdAt": 1,
        "origAmount": 1
    }).sort({
        "createdAt": -1
    })

    txns = list(txns)

    if len(txns) <= 0:
        continue

    last_txn = txns[0]

    if last_txn['createdAt'] > datetime(2022, 12, 1):
        continue

    pocket = client\
    .get_database("ayapay")\
    .get_collection("pocket")\
    .find_one({
        "user": str(cust_id),
        "client": "customer",
        "status": "active"
    }, {
        "_id": 0,
        "balance": 1
    })

    if pocket == None:
        pocket = {"balance": 0}

    bankacc = client\
    .get_database("ayapay")\
    .get_collection("bankaccount")\
    .find_one({
        "user": str(cust_id),
        "client": "customer",
        "status": "linked",
        "state": "active"
    }, {
        "_id": 0,
        "accountName": 1,
        "accountNo": 1,
        "accountNoShowed": 1
    })

    if bankacc == None:
        bankacc = {"accountName": "N/A", "accountNo": "N/A", "accountNoShowed": "N/A"}

    cust.update(last_txn)
    cust.update(pocket)
    cust.update(bankacc)

    writer.writerow(cust)
    counter += 1

    if counter % 10 == 0:
        f.close()
        break
#         f = open(f"InactiveCustomersPart{str(int(counter/100+1))}.csv", "w", newline="")
#         writer = csv.DictWriter(f, field_names)
#         writer.writeheader()

f.close()
